package br.com.tqi.desing.pattern.builder;

public class Address {

	private String name;
	private String city;
	private String state;
	private String postalCode;

	public Address(String name, String city, String state, String postalCode) {
		this.name = name;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Override
	public String toString() {
		return String.format("Address [name=%s, city=%s, state=%s, postalCode=%s]", name, city, state, postalCode);
	}
}