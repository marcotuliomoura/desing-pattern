package br.com.tqi.desing.pattern.builder.utils;

import java.time.LocalDate;
import java.util.function.Consumer;

import br.com.tqi.desing.pattern.builder.Address;
import br.com.tqi.desing.pattern.builder.Gender;
import br.com.tqi.desing.pattern.builder.Person;

public class PersonBuilder {

	public String firstName;
	public String lastName;
	public LocalDate birthDate;
	public String phone;
	public String email;
	public Gender gender;
	public Address address;
	public boolean isActive;

	public PersonBuilder with(Consumer<PersonBuilder> builderFunction) {
		builderFunction.accept(this);
		return this;
	}

	public Person build() {
		return new Person(firstName, lastName, birthDate, phone, email, gender, address, isActive);
	}
}