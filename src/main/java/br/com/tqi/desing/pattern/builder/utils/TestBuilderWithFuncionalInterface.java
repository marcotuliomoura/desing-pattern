package br.com.tqi.desing.pattern.builder.utils;

import java.time.LocalDate;
import java.time.Month;

import br.com.tqi.desing.pattern.builder.Gender;
import br.com.tqi.desing.pattern.builder.Person;

public class TestBuilderWithFuncionalInterface {

	public static void main(String args[]) {

		Person person = new PersonBuilder().with($ -> {
			$.firstName = "Marco Tulio";
			$.lastName = "Moura";
			$.birthDate = LocalDate.of(1979, Month.MARCH, 7);
			$.phone = "9999-9999";
			$.gender = Gender.M;
			$.email = "marco.moura@tqi.com.br";
			$.isActive = true;
			$.address = new AddressBuilder().with($_address -> {
				$_address.name = "Av. Maranhão";
				$_address.postalCode = "38400-136";
				$_address.city = "Uberlandia";
			}).build();
		}).build();
	}
}