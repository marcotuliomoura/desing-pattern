package br.com.tqi.desing.pattern.builder.lombok;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Endereco {

	private String nome;
	private String cidade;
	private String estado;
	private String cep;

}
