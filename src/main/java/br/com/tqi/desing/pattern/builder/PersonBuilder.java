package br.com.tqi.desing.pattern.builder;

import java.time.LocalDate;

public class PersonBuilder {

	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	private String phone;
	private String email;
	private Gender gender;
	private Address address;
	private boolean isActive;

	public PersonBuilder firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public PersonBuilder lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public PersonBuilder birthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
		return this;
	}

	public PersonBuilder phone(String phone) {
		this.phone = phone;
		return this;
	}

	public PersonBuilder email(String email) {
		this.email = email;
		return this;
	}

	public PersonBuilder gender(Gender gender) {
		this.gender = gender;
		return this;
	}

	public PersonBuilder address(Address address) {
		this.address = address;
		return this;
	}

	public PersonBuilder isActive(boolean isActive) {
		this.isActive = isActive;
		return this;
	}

	public Person build() {
		return new Person(firstName, lastName, birthDate, phone, email, gender, address, isActive);
	}
}