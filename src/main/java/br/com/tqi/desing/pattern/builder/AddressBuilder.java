package br.com.tqi.desing.pattern.builder;

public class AddressBuilder {

	private String name;
	private String city;
	private String state;
	private String postalCode;

	public AddressBuilder withAddress(String name) {
		this.name = name;
		return this;
	}

	public AddressBuilder withCity(String city) {
		this.city = city;
		return this;
	}

	public AddressBuilder withState(String state) {
		this.state = state;
		return this;
	}

	public AddressBuilder withPostalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public Address build() {
		return new Address(name, city, state, postalCode);
	}
}