package br.com.tqi.desing.pattern.builder.utils;

import java.util.function.Consumer;

import br.com.tqi.desing.pattern.builder.Address;

public class AddressBuilder {

	public String name;
	public String city;
	public String state;
	public String postalCode;

	public AddressBuilder with(Consumer<AddressBuilder> builderFunction) {
		builderFunction.accept(this);
		return this;
	}

	public Address build() {
		return new Address(name, city, state, postalCode);
	}
}