package br.com.tqi.desing.pattern.builder;

import java.time.LocalDate;
import java.time.Month;

public class TestBuilder {

	public static void main(String args[]) {

		Address address = new Address("Av. Maranhão, 2399", "Uberlandia", "MG", "38400-136");

		Person person = new Person("Marco Tulio", "Moura", LocalDate.of(1979, Month.MARCH, 7), "9999-9999",
				"marco.moura@tqi.com.br", Gender.M, address, true);

		Person personBuilder = new PersonBuilder()
				.firstName("Marco Tulio")
				.lastName("Moura")
				.birthDate(LocalDate.of(1979, Month.MARCH, 7))
				.phone("9999-9999")
				.email("marco.moura@tqi.com.br")
				.gender(Gender.M)
				.address(new AddressBuilder()
						.withAddress("Av. Maranhão, 2399")
						.withCity("Uberlandia")
						.withState("MG")
						.withPostalCode("38400-136")
						.build())
				.build();
	}
}