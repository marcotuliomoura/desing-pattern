package br.com.tqi.desing.pattern.builder.lombok;

import java.time.LocalDate;
import java.time.Month;

public class TestePersonLombok {
	
	public static void main(String args[]) {
		
		Pessoa pessoa = Pessoa.builder()
				.nome("Marco Túlio")
				.sobrenome("Moura")
				.email("marcotuliomoura@gmail.com")
				.telefone("9999-9999")
				.dataNascimento(LocalDate.of(1979, Month.MARCH, 7))
				.genero(Genero.M)
				.endereco(Endereco.builder()
						.nome("Av. Maranhão, 2349")
						.cep("38400-408")
						.cidade("Uberlandia")
						.estado("MG")
						.build())
				.build();
		System.out.println(pessoa);
	}
	
}
