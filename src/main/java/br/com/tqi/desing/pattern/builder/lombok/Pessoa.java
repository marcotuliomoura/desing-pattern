package br.com.tqi.desing.pattern.builder.lombok;

import java.time.LocalDate;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Pessoa {

	private String nome;
	private String sobrenome;
	private LocalDate dataNascimento;
	private String telefone;
	private String email;
	private Genero genero;
	private Endereco endereco;
	private boolean isActive;

}