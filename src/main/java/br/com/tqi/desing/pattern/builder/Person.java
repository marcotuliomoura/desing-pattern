package br.com.tqi.desing.pattern.builder;

import java.time.LocalDate;

public class Person {

	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	private String phone;
	private String email;
	private Gender gender;
	private Address address;
	private boolean isActive;

	public Person(String firstName, String lastName, LocalDate birthDate, String phone, String email, Gender gender,
			Address address, boolean isActive) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.phone = phone;
		this.email = email;
		this.gender = gender;
		this.address = address;
		this.isActive = isActive;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return String.format(
				"Person [firstName=%s, lastName=%s, birthDate=%s, phone=%s, email=%s, gender=%s, address=%s, isActive=%s]",
				firstName, lastName, birthDate, phone, email, gender, address, isActive);
	}

}